﻿namespace NavalCombat
{
    public enum Orientation
    {
        Vertical,
        Horizontal
    }

    public class Ship : AddressRange
    {
        public Address Bow
        {
            get { return Start; }
            set { Start = value; }
        }

        public Address Stern
        {
            get { return End; }
            set { End = value; }
        }

        public PlayField Field { get; }

        public char this[Address address]
        {
            get
            {
                return Field[address];
            }
        }

        internal Ship(PlayField Field, Address Bow, Address Stern) : base(Bow, Stern)
        {
            if (Stern.Col - Bow.Col > 4 || Stern.Row - Bow.Row > 4)
                throw new ArgumentException("Длина корабля должна находиться в интервале 1-4");
            if (Stern.Col - Bow.Col > 1 && Stern.Row - Bow.Row > 1)
                throw new ArgumentException("Ширина корабля должна быть равна 1");

            this.Field = Field;
        }

        public Orientation Orientation
        {
            get
            {
                return (Width == 1 ? Orientation.Vertical : Orientation.Horizontal);
            }
        }

        public byte DeckCount => Count;

        public Address[] Decks => Cells;

        public char State
        {
            get
            {
                int inj_qnt = 0;
                int dq = Decks.Length;

                if (this[Decks[0]] == 'K')
                    return 'K';

                for (int i = 0; i < dq; i++)
                    if (this[Decks[i]] == 'I')
                        inj_qnt++;

                return (inj_qnt == 0 ? 'H' : (dq == inj_qnt ? 'K' : 'I'));
            }
        }
    }
}
