﻿using Newtonsoft.Json;

namespace NavalCombat;

public enum Turn
{
    Mine,
    Foes
}

struct SavedGame
{
    public char[,] MyField;
    public char[,] FoeField;
    public Turn Turn;
}

public class Game
{
    public Guid guid { get; private set; }
    public Turn CurrentTurn { get; private set; }

    public PlayField BattleField => CurrentTurn == Turn.Mine ? FoeField : MyField;

    public PlayField MyField;
    public PlayField FoeField;

    public Game(Turn FirstTurn = Turn.Mine)
    {
        (MyField, FoeField) = (new(), new());
        guid = Guid.NewGuid();
        FoeField.PutShips();
        if (FirstTurn == Turn.Foes)
            MyField.PutShips();
        CurrentTurn = FirstTurn;
    }

    public bool Started => MyField.Complete;

    public Turn? Winner
    {
        get
        {
            if (20 == MyField.Where(a => a == 'K').Count())
                return Turn.Foes;
            else if (20 == FoeField.Where(a => a == 'K').Count())
                return Turn.Mine;
            else
                return null;
        }
    }

    public bool Over
    {
        get { return Winner != null; }
    }

    public string Save(string FileName)
    {
        SavedGame g = new()
        {
            MyField = MyField.ToArray(),
            FoeField = FoeField.ToArray(),
            Turn = CurrentTurn
        };
        using (StreamWriter sw = new(FileName))
        {
            sw.Write(JsonConvert.SerializeObject(g));
        }

        return FileName;
    }

    public string Save()
    {
        string FileName = Path.GetTempPath() + guid + ".mcs";
        return Save(FileName);
    }

    public void LoadFromFile(string FileName)
    {
        using (StreamReader sr = new(FileName))
        {
            string s = sr.ReadToEnd();
            try
            {
                var g = JsonConvert.DeserializeObject<SavedGame>(s);
            
                MyField = new(g.MyField);
                FoeField = new(g.FoeField);
                CurrentTurn = g.Turn;
            }
            catch
            {
                ;
            }
        }
    }

    private void ToggleTurn()
    {
        CurrentTurn = CurrentTurn == Turn.Mine ? Turn.Foes : Turn.Mine;
    }

    public Shot Shoot(Address address)
    {
        Shot shot = new(CurrentTurn == Turn.Mine ? Turn.Foes : Turn.Mine, address, BattleField.Shoot(address));

        if (shot.Result == 'B')
        {
            ToggleTurn();

            List<Shot> ReturnShots = new();

            Address? a;
            while ((CurrentTurn == Turn.Foes) && ((a = BattleField.GetNextShot()) != null) && !Over)
                ReturnShots.Add(new Shot(CurrentTurn == Turn.Mine ? Turn.Foes : Turn.Mine, a, Shoot(a).Result));

            shot.ReturnShots = ReturnShots.ToArray();
        }
        shot.Winner = Winner;

        return shot;
    }

    public Shot Shoot()
    {
        if (!MyField.Complete)
            throw new ApplicationException("Your field is not complete");

        var a = BattleField.GetNextShot();
        if (a != null)
            return Shoot(a);
        else
            throw new ApplicationException("Something went wrong");
    }

    public void ReStart(Turn FirstTurn = Turn.Mine)
    {
        MyField.Clear();
        FoeField.Clear();

        FoeField.PutShips();

        CurrentTurn = FirstTurn;
    }
}
