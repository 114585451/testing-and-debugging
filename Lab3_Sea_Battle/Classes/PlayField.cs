using Matrices;

namespace NavalCombat;

public class PlayField : Matrix<char>
{
    public PlayField() : base(10, 10)
    {
        Clear();
    }

    public PlayField(Matrix<char> m) : base(m.ToArray()) {; }

    public bool Complete
    {
        get
        {
            return (20 == this.Where(a => a == 'H' || a == 'I' || a == 'K').Count());
        }
    }

    public char this[char Horizontal, int Vertical]
    {
        get { return (char)Data[Vertical - 1, Address.HToInt(Horizontal) - 1]; }
        set { Data[Vertical - 1, Address.HToInt(Horizontal) - 1] = value; }
    }

    public char this[Address Address]
    {
        get { return this[Address.Horizontal, Address.Vertical]; }
        set { this[Address.Horizontal, Address.Vertical] = value; }
    }

    public Address? GetNextShot()
    {
        Address[] af = GetAvailableShots();

        Address? _injured = Cells.Where(a => this[a] == 'I').FirstOrDefault();
        if (_injured != null)
        {
            var ship = GetShipAt(_injured);
            var InjQnt = ship?.Decks.Where(a => this[a] == 'I').Count() ?? 0;
            switch (InjQnt)
            {
                case 1:
                    af = af.Where(a =>
                                 (a == _injured.Move(MoveDirection.Left)) ||
                                 (a == _injured.Move(MoveDirection.Right)) ||
                                 (a == _injured.Move(MoveDirection.Down)) ||
                                 (a == _injured.Move(MoveDirection.Up))).ToArray();
                    break;

                default:
                    var InjRange = AddressRange.FromArray(ship?.Decks.Where(a => this[a] == 'I')?.ToArray() ?? new Address[0]);

                    af = af.Where(a =>
                                 (ship?.Orientation == Orientation.Horizontal
                                  ?
                                  (a == InjRange.Start.Move(MoveDirection.Left)) ||
                                  (a == InjRange.End.Move(MoveDirection.Right))
                                  :
                                  (a == InjRange.Start.Move(MoveDirection.Up)) ||
                                  (a == InjRange.End.Move(MoveDirection.Down)))).ToArray();
                    break;
            }
        }

        var r = new Random();
        if (af.Length == 0)
            return null;
        else
        {
            var a = af[r.Next(0, af.Length)];
            return a;
        }
    }

    public override char this[int Row, int Col]
    {
        get { return this[new Address(Row, Col)]; }
        set { this[new Address(Row, Col)] = value; }
    }

    internal char Shoot(Address address)
    {
        if ((this[address] != 'H') && (this[address] != 'E'))
            throw new Exception("Duplicate shot");

        this[address] =
            this[address] switch
            {
                'H' => 'I',
                'I' => 'I',
                _ => 'B'
            };

        if (this[address] != 'B')
        {
            var ship = GetShipAt(address);
            if ((ship != null) && (ship.State == 'K'))
            {
                for (int i = 0; i < ship.Decks.Length; i++)
                    this[ship.Decks[i]] = 'K';
            }
        }

        return this[address];
    }

    public Address[] EmptyFields
    {
        get
        {
            List<Address> av_f = new();
            for (int r = 0; r < 10; r++)
                for (int c = 0; c < 10; c++)
                {
                    if (this[r, c] == 'E')
                        av_f.Add(new Address(r, c));
                }
            return av_f.ToArray();
        }
    }

    public bool ShotAvailable(Address address)
    {
        if ((this[address] != 'E') && (this[address] != 'H'))
            return false;

        var Neibours = address.GetNeibours();
        if (Neibours.Where(a => (this[a] == 'K')).Count() > 0)
            return false;

        var InjuredNeibours = Neibours.Where(a => (this[a] == 'I'));
        
        if (InjuredNeibours.Count() == 0)
            return true;
        else
        {
            foreach (var injured in InjuredNeibours)
            {
                (int h, int v) = ((address - injured).H, (address - injured).V);
                
                if (h * v != 0) // diagonal
                    return false;

                var ship = GetShipAt(injured);

                if (ship?.Decks.Count(a => this[a] == 'I') == 1)
                    return true;
                
                if (h == 0 && ship?.Orientation != Orientation.Vertical)
                    return false;
                if (v == 0 && GetShipAt(injured)?.Orientation != Orientation.Horizontal)
                    return false;
            }
        }

        return true;
    }

    public Address[] Cells
    {
        get
        {
            return new AddressRange("�1", "�10").Cells;
        }
    }

    public Address[] GetAvailableShots()
    {
        return Cells.Where(a => ShotAvailable(a)).ToArray();
    }

    public Ship? GetShipAt(Address address)
    {
        if ((this[address] == 'E') || (this[address] == 'B'))
            return null;

        Address Bow = address;
        Address Stern = address;

        int i = 1;

        while ((address.Col - i >= 0) && (this[address.Row, address.Col - i] != 'E')
            && ((address.Col - i >= 0) && this[address.Row, address.Col - i] != 'B'))
        {
            Bow = new Address(address.Row, address.Col - i);
            i++;
        }

        i = 1;
        while ((address.Row - i >= 0) && (this[address.Row - i, address.Col] != 'E')
            && ((address.Row - i >= 0) && this[address.Row - i, address.Col] != 'B'))
        {
            Bow = new Address(address.Row - i, address.Col);
            i++;
        }

        i = 1;
        while ((address.Col + i <= 9) && (this[address.Row, address.Col + i] != 'E')
            && ((address.Col + i <= 9) && this[address.Row, address.Col + i] != 'B'))
        {
            Stern = new Address(address.Row, address.Col + i);
            i++;
        }

        i = 1;
        while ((address.Row + i <= 9) && (this[address.Row + i, address.Col] != 'E')
            && ((address.Row + i <= 9) && this[address.Row + i, address.Col] != 'B'))
        {
            Stern = new Address(address.Row + i, address.Col);
            i++;
        }

        return new Ship(this, Bow, Stern);
    }

    public bool Empty(AddressRange range)
    {
        return range.Cells.Where(c => (this[c] != 'E')).Count() == 0;
    }

    public Ship PutShip(AddressRange range)
    {
        if (!Empty(AddressRange.FromArray(range.Neighbors.Union(range.Cells).ToArray())))
            throw new ApplicationException($"Can't put ship on [{range.Start},{range.End}]. Some cells or border are busy");

        foreach (var a in range.Cells)
            this[a] = 'H';

        var ship = GetShipAt(range.Start);
        if (ship == null)
            throw new ApplicationException("Something went wrong when puting ship");
        else
            return ship;
    }

    public Ship PutShip(Address Bow, Address Stern)
    {
        return PutShip(new AddressRange(Bow, Stern));
    }

    public Ship PutShip(Address Bow, int DecksQnt, Orientation Orientation)
    {
        int r = Bow.Row + (DecksQnt - 1) * (Orientation == Orientation.Vertical ? 1 : 0);
        int c = Bow.Col + (DecksQnt - 1) * (int)Orientation;
        var range = new AddressRange(Bow, new Address(r, c));
        return PutShip(range);
    }

    public Ship? PutShip(byte DecksQnt)
    {
        var rnd = new Random();
        Address CurBow;
        List<Address> AvailableFields;

        var ro = rnd.Next(0, 2);
        Orientation[] orientations = { (Orientation)ro, (Orientation)(ro == 0 ? 1 : 0) };

        foreach (var o in orientations)
        {
            AvailableFields = EmptyFields.ToList();
            Address es;

            es = (o == Orientation.Vertical
                  ? new Address('�', 10 - DecksQnt + 1)
                  : new Address(Address.IntToH(10 - DecksQnt + 1), 1));

            AvailableFields = AvailableFields.Except(new AddressRange(es, "�10").Cells).ToList();

            while (AvailableFields.Count > 0)
            {
                CurBow = AvailableFields[rnd.Next(0, AvailableFields.Count)];
            
                var range = new AddressRange(
                    CurBow,
                    new Address(CurBow.Row + (DecksQnt - 1) * (o == Orientation.Vertical ? 1 : 0),
                                CurBow.Col + (DecksQnt - 1) * (o == Orientation.Horizontal ? 1 : 0)));
                
                for (int i = range.Count - 1; i >= 0; i--)
                    AvailableFields.Remove(range.Cells[i]);

                var Area = AddressRange.FromArray(range.Neighbors.Union(range.Cells).ToArray());
                if (Empty(Area))
                    return PutShip(range);
            }
        }

        return null;
    }

    public void Clear()
    {
        for (int i = 0; i < RowCount; i++)
            for (int j = 0; j < RowCount; j++)
                this[i, j] = 'E';
    }

    public Ship[] PutShips()
    {
        Clear();
        var ShipDecks = new List<byte> { 4, 3, 3, 2, 2, 2, 1, 1, 1, 1 };
        List<Ship> ships = new();
        Ship? ship;

        foreach (var q in ShipDecks)
        {
            ship = PutShip(q);
            if (ship == null)
                throw new ApplicationException("Can't put ships :(");
            else
                ships.Add(ship);
        }

        return ships.ToArray();
    }

    public string ToHtml(string Id = "")
    {
        string s = $"<table id='{Id}' class='{(Complete ? "Complete" : "")}'>";
        s += "<tr>";
        s += "<td class='header'header>&nbsp;</td>";
        for (int i = 0; i < 10; i++)
            s += $"<td class='header h'>{Address.IntToH(i+1)}</td>";
        s += "</tr>";
        
        for (int r = 0; r < RowCount; r++)
        {
            s += "<tr>";
            s += $"<td class='header v'>{r+1}</td>";
            for (int c = 0; c < ColCount; c++)
            {
                var address = new Address(r, c);
                Ship? Ship;
                if (r >= 0)
                {
                    Ship = GetShipAt(new Address(r, c));
                    string ShipName = "";
                    if (Ship != null)
                    {
                        ShipName += (this[r, c]) switch
                        {
                            'H' => Ship.Bow.ToString(),
                            'I' => Ship.Bow.ToString(),
                            '�' => Ship.Bow.ToString(),
                            _ => ""
                        };
                    }
                    s += $"<td data-address='{address}' data-ship='{ShipName}' class = 'cell {(char)this[r, c]}'></td>";
                }                
               
            }
            s += "</tr>";
        }
        s += "</table>";
        return s;
    }
}




