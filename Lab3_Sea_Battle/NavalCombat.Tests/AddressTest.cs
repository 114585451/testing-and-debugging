﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavalCombat.Tests
{
    [TestFixture(TestOf = typeof(Address), Description = "Address type tests")]
    internal class AddressTest
    {
        List<char> chars = new() { 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'К', };


        [Test]
        public void TestCharToIntConvertation()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { Address.HToInt('Л'); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { Address.HToInt('D'); });
            for (int i = 0; i < chars.Count; i++)
                Assert.That(Address.HToInt(chars[i]) == i + 1);
        }

        [Test]
        public void TestIntToCharConvertation()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { Address.IntToH(-1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { Address.IntToH(0); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { Address.IntToH(11); });
            for (int i = 0; i < 10; i++)
                Assert.That(Address.IntToH(i + 1) == chars[i]);
        }

        [Test]
        public void TestConstructors()
        {
            Address a;

            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address('Й', 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address('S', 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address('Л', 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address('А', 11); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address('К', 11); });

            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address(-1, 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address(11, 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address(1, -1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address(1, 11); });

            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address("S2"); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address("Ф1"); }); ;
            Assert.Throws<ArgumentOutOfRangeException>(() => { a = new Address("Б11"); }); ;

            foreach (char c in chars)
                for (int i = 1; i <= 10; i++)
                {
                    a = new Address(c, i);
                    Assert.That(a.ToString() == c + "" + i);
                }

            for (int r = 0; r < 10; r++)
                for (int c = 0; c < 10; c++)
                {
                    a = new Address(r, c);
                    Assert.That(a.ToString() == Address.IntToH(c + 1) + "" + (r + 1));
                }

            foreach (char c in chars)
                for (int i = 1; i <= 10; i++)
                {
                    a = new Address(c + "" + i);
                    Assert.That(a.ToString() == c + "" + i);
                }
        }

        [Test]
        public void TestRowAndCol()
        {
            Address a;

            for (int r = 0; r < 10; r++)
                for (int c = 0; c < 10; c++)
                {
                    a = new Address(r, c);
                    Assert.That(a.Col == c);
                    Assert.That(a.Row == r);
                }
        }

        [Test]
        public void TestVerticalAndHorizontal()
        {
            Address a;

            foreach (char c in chars)
                for (int i = 1; i <= 10; i++)
                {
                    a = new Address(c, i);

                    Assert.That(a.Vertical == i);
                    Assert.That(a.Horizontal == c);
                }
        }

        [Test]
        public void TestNeibours()
        {
            CollectionAssert.AreEquivalent(new Address[] { "А2", "Б1", "Б2" }, (new Address("А1")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "А1", "А3", "Б1", "Б2", "Б3" }, (new Address("А2")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "А1", "А2", "Б2", "В1", "В2" }, (new Address("Б1")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "И1", "И2", "К2" }, (new Address("К1")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "А9", "Б9", "Б10" }, (new Address("А10")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "А10", "А9", "Б9", "В9", "В10" }, (new Address("Б10")).GetNeibours());
            CollectionAssert.AreEquivalent(new Address[] { "И10", "И9", "К9" }, (new Address("К10")).GetNeibours());
        }

        [Test]
        public void TestMove()
        {
            Assert.That(((Address)"Б2").Move(2, 3), Is.EqualTo((Address)"Г5"));
            Assert.That(((Address)"Б2").Move(-1, 0), Is.EqualTo((Address)"А2"));
            Assert.That(((Address)"Б2").Move(0, -1), Is.EqualTo((Address)"Б1"));

            Assert.That(((Address)"Б2").Move(MoveDirection.Left), Is.EqualTo((Address)"А2"));
            Assert.That(((Address)"Б2").Move(MoveDirection.Right), Is.EqualTo((Address)"В2"));
            Assert.That(((Address)"Б2").Move(MoveDirection.Up), Is.EqualTo((Address)"Б1"));
            Assert.That(((Address)"Б2").Move(MoveDirection.Down), Is.EqualTo((Address)"Б3"));
            
            Assert.That(((Address)"А1").Move(MoveDirection.Left), Is.EqualTo(null));
            Assert.That(((Address)"А1").Move(MoveDirection.Right), Is.EqualTo((Address)"Б1"));
            Assert.That(((Address)"А1").Move(MoveDirection.Up), Is.EqualTo(null));
            Assert.That(((Address)"А1").Move(MoveDirection.Down), Is.EqualTo((Address)"А2"));

            Assert.That(((Address)"А2").Move(MoveDirection.Left), Is.EqualTo(null));

            Assert.That(((Address)"К1").Move(MoveDirection.Left), Is.EqualTo((Address)"И1"));
            Assert.That(((Address)"К1").Move(MoveDirection.Right), Is.EqualTo(null));
            Assert.That(((Address)"К1").Move(MoveDirection.Up), Is.EqualTo(null));
            Assert.That(((Address)"К1").Move(MoveDirection.Down), Is.EqualTo((Address)"К2"));

            Assert.That(((Address)"К10").Move(MoveDirection.Left), Is.EqualTo((Address)"И10"));
            Assert.That(((Address)"К10").Move(MoveDirection.Right), Is.EqualTo(null));
            Assert.That(((Address)"К10").Move(MoveDirection.Up), Is.EqualTo((Address)"К9"));
            Assert.That(((Address)"К10").Move(MoveDirection.Down), Is.EqualTo(null));

            Assert.That(((Address)"Г5")
                .Move(MoveDirection.Down)?
                .Move(MoveDirection.Down)?
                .Move(MoveDirection.Right)?
                .Move(MoveDirection.Right)?
                .Move(MoveDirection.Up)?
                .Move(MoveDirection.Left), Is.EqualTo((Address)"Д6"));

        }
    }
}
