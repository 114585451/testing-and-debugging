﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavalCombat.Tests
{
    [TestFixture(TestOf = typeof(AddressRange), Description = "Address range tests")]
    internal class AddressRangeTest
    {
        [Test]
        public void TestConstructors()
        {
            AddressRange ar;

            ar = new("А1", "Г1");
            CollectionAssert.AreEquivalent(new Address[] { "А1", "Б1", "В1", "Г1" }, ar.Cells);
            ar = new("А1", "А4");
            CollectionAssert.AreEquivalent(new Address[] { "А1", "А2", "А3", "А4" }, ar.Cells);
            ar = new("А1", "Г2");
            CollectionAssert.AreEquivalent(new Address[] { "А1", "Б1", "В1", "Г1", "А2", "Б2", "В2", "Г2" }, ar.Cells);

            Assert.Throws<ArgumentOutOfRangeException>(() => { ar = new("А8", 4, 1); });
            Assert.Throws<ArgumentOutOfRangeException>(() => { ar = new("З9", 1, 4); });
        }

        [Test]
        public void TestWidthHeightCount()
        {
            AddressRange ar;

            ar = new("Г1", "Ж1");
            Assert.That(ar.Width == 4);
            Assert.That(ar.Height == 1);
            Assert.That(ar.Count == ar.Width * ar.Height);

            ar = new("Г1", "Г2");
            Assert.That(ar.Width == 1);
            Assert.That(ar.Height == 2);
            Assert.That(ar.Count == ar.Width * ar.Height);

            ar = new("А1", "В3");
            Assert.That(ar.Width == 3);
            Assert.That(ar.Height == 3);
            Assert.That(ar.Count == ar.Width * ar.Height);

            ar = new("З9", "К9");
            Assert.That(ar.Width == 3);
            Assert.That(ar.Height == 1);
            Assert.That(ar.Count == ar.Width * ar.Height);
        }

        [Test]
        public void TestNeighbors()
        {
            CollectionAssert.AreEquivalent(
                new Address[] { "В1", "В2", "Г2", "Д2", "Е2", "Ж2", "З2", "З1" },
                (new AddressRange("Г1", "Ж1").Neighbors));

            CollectionAssert.AreEquivalent(
                new Address[6] { "А5", "Б1", "Б2", "Б3", "Б4", "Б5" },
                new AddressRange("А1", "А4").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[6] { "В1", "В2", "Г2", "Д2", "Е2", "Е1" },
                new AddressRange("Г1", "Д1").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[7] { "З2", "И2", "К2", "З3", "З4", "И4", "К4" },
                new AddressRange("И3", "К3").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[12] { "Д3", "Д4", "Д5", "Д6", "Д7", "Е3", "Е7", "Ж3", "Ж4", "Ж5", "Ж6", "Ж7" },
                new AddressRange("Е4", "Е6").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[6] { "А9", "Б9", "В9", "Г9", "Д9", "Д10" },
                new AddressRange("А10", "Г10").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[6] { "И7", "И8", "И9", "И10", "К7", "К10" },
                new AddressRange("К8", "К9").Neighbors);

            CollectionAssert.AreEquivalent(
                new Address[8] { "Б7", "Б8", "Б9", "В7", "В9", "Г7", "Г8", "Г9" },
                new AddressRange("В8", "В8").Neighbors);
        }

        [Test]
        public void TestFromArray()
        {
            CollectionAssert.AreEquivalent(
                new Address[] { "А1", "Б1", "В1", "Г1", "А2", "Б2", "В2", "Г2" },
                AddressRange.FromArray(new Address[] { "А1", "Б1", "В1", "Г1", "А2", "Б2", "В2", "Г2" }).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "В3", "В4", "В5", "В6" },
                AddressRange.FromArray(new Address[] { "В3", "В4", "В5", "В6" }).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "В2", "В3", "Г2", "Г3" },
                AddressRange.FromArray(new Address[] { "Б2", "Б3", "В2", "В3", "Г2", "Г3" }).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "Б4", "Б5",
                            "В2", "В3", "В4", "В5",
                            "Г2", "Г3", "Г4", "Г5",},
                AddressRange.FromArray(new Address[] { "Б2", "Б3", "Б4", "Б5",
                                                       "В2", "В3", "В4", "В5",
                                                       "Г2", "Г3", "Г4", "Г5",}).Cells);

            Assert.Throws<ArgumentException>(() => {
                AddressRange.FromArray(
                    new Address[] { "Б2", "В3", "Г2", "Г2" });
            });

            Assert.Throws<ArgumentException>(() => {
                AddressRange.FromArray(
                    new Address[] { "А1", "А4", "Б1", "Б1", "Б4", "Б4" });
            });
        }

        [Test]
        public void TestUnion()
        {
            CollectionAssert.AreEquivalent(
                new Address[] { "А1", "Б1", "В1", "Г1", "А2", "Б2", "В2", "Г2" },
                AddressRange.Union(new AddressRange("А1", "Г1"), new AddressRange("А2", "Г2")).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "В3", "В4", "В5", "В6" },
                AddressRange.Union(new AddressRange("В3", "В3"), new AddressRange("В4", "В6")).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "В2", "В3", "Г2", "Г3" },
                AddressRange.Union(new AddressRange("Б2", "В3"), new AddressRange("Г2", "Г3")).Cells);

            CollectionAssert.AreEquivalent(
                new Address[] { "Б2", "Б3", "Б4", "Б5",
                            "В2", "В3", "В4", "В5",
                            "Г2", "Г3", "Г4", "Г5",},
                AddressRange.Union(
                    new AddressRange("Б2", "В3"),
                    new AddressRange("Г2", "Г2"),
                    new AddressRange("Г3", "Г3"),
                    new AddressRange("Б4", "В4"),
                    new AddressRange("Г4", "Г4"),
                    new AddressRange("Б5", "В5"),
                    new AddressRange("В5", "Г5")).Cells);

            Assert.Throws<ArgumentException>(() => {
                AddressRange.Union(
                    new AddressRange("Б2", "В3"),
                    new AddressRange("Г2", "Г2"));
            });

            Assert.Throws<ArgumentException>(() => {
                AddressRange.Union(
                    new AddressRange("А1", "А4"),
                    new AddressRange("Б1", "Б1"),
                    new AddressRange("Б4", "Б4"));
            });
        }
    }
}
