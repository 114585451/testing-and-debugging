﻿function saveGame() {
    $.ajax({
        type: "GET",
        url: "?handler=Save",
        contentType: "application/json; charset=utf-8",
        headers:
        {
            "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (response) {
            alert("Игра сохранена");
        },
        failure: function (response) {
            alert("Ошибка сохранения игры. " + response);
        }
    });
}

function putShips() {
    if (!$('#battlefield').hasClass('started')) {
        $.ajax({
            type: "GET",
            url: "?handler=PutShips",
            contentType: "application/json; charset=utf-8",
            //dataType: "json",
            headers:
            {
                "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
            },
            success: function (MyFieldHtml) {
                $("#field0").html(MyFieldHtml);
                $("#battlefield").addClass('complete');
                $('#status').text("Сделайте выстрел");
            },
            failure: function (response) {
                alert("Ошибка расстановки кораблей. " + response);
            }
        });
    }
}

function newGame() {    
    $.ajax({
        type: "GET",
        url: "?handler=ReStart",
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        headers:
        {
            "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function () {
            window.location.reload();
        },
        failure: function (response) {
            alert("Ошибка начала новой игры. " + response);
        }
    });
}

function processShot(shot) {    
    let element = $('#field' + shot.field + ' .cell[data-address=' + shot.address.horizontal + shot.address.vertical + ']');
    $(element).removeClass('H');
    $(element).removeClass('E');
    if (shot.result != 'K') {
        $(element).addClass(shot.result);
    }
    else {
        let ship = $(element).data('ship');
        $('#field' + shot.field + ' .cell[data-ship=' + ship + ']').each(function (index, cell) {
            $(cell).removeClass('H');
            $(cell).removeClass('I');
            $(cell).addClass('K');
        });
    }
    $('#battlefield').addClass('started');
}

function getNextShot() {
    $.ajax({
        type: "GET",
        url: "?handler=NextShot",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers:
        {
            "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (address) {
            if (address != null) {
                return address;
            }
            else {
                return false;
            }
        },
        failure: function (response) {
            alert(response);
        }
    });
}


function autoPlay() {
    /*while (a = getNextShot())
        shoot(a);*/
    $.ajax({
        type: "GET",
        url: "?handler=NextShot",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers:
        {
            "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (address) {
            if (address != null) {
                $.ajax({
                    type: "POST",
                    url: "?handler=Send&address=" + address,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    headers:
                    {
                        "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
                    },
                    success: function (shot) {
                        processShot(shot);

                        var time = 150;
                        $(shot.returnShots).each(function (index, returnShot) {
                            setTimeout(() => {
                                processShot(returnShot);
                            }, time)
                            time += 150
                        });

                        if (shot.winner != null) {
                            $('#status').text("Вы " + (shot.winner ? 'проиграли!' : 'выиграли!'));
                            $('#status').addClass((shot.winner ? 'looser' : 'winner'));
                            $('#battlefield').addClass('over')
                        }
                        else {
                            setTimeout(() => {
                                autoPlay();
                            }, 150)
                        }
                    },
                    failure: function (response) {
                        alert(response);
                    }
                });
            }
            else {
                return false;
            }
        },
        failure: function (response) {
            alert(response);
        }
    });
}

function shoot(addr) {
    $.ajax({
        type: "POST",
        url: "?handler=Send&address="+addr,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers:
        {
            "RequestVerificationToken": $('input:hidden[name="__RequestVerificationToken"]').val()
        },
        success: function (shot) {            
            processShot(shot);

            var time = 500;

            $(shot.returnShots).each(function (index, returnShot) {

                setTimeout(() => {
                    processShot(returnShot);
                }, time);

                time += 500;
            });

            if (shot.winner != null) {
                $('#status').text("Вы " + (shot.winner ? 'проиграли!' : 'выиграли!'));
                $('#status').addClass((shot.winner ? 'looser' : 'winner'));
                $('#battlefield').addClass('over')
            }
        },
        failure: function (response) {
            alert(response);
        }
    });
}

$(document).ready(function () {    
    /*if (typeof(getCookie('ASP.NET_SessionId') == typeof('undefined')))
        setCookie('player_id', getCookie('ASP.NET_SessionId'), { expires: 365 * 10 });*/

    $('#field1 td').click(function () {
        if ($('#battlefield').hasClass('complete')
            && !($('#status').hasClass('winner'))
            && !($('#status').hasClass('looser')))
        {
            if ($(this).hasClass('H') || $(this).hasClass('E'))
                shoot($(this).data('address'));
        }
    });

})