﻿namespace Matrices;

public interface IMatrix<T>
{
    public int RowCount { get; }
    public int ColCount { get; }
    public string? Name { get; set; }
    public T[,] ToArray();
    public T[] Values { get; }
    public T this[int r, int c] { get; set; }
    public bool IsSymmetric { get; }
    public bool IsSquare { get; }
}
