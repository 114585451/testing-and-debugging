#pragma once
#include <iostream>
#include <vector>
#include <stdexcept>

class MatrixMultiplier {
public:
    static std::vector<std::vector<int>> MultiplyMatrices(const std::vector<std::vector<int>>& first, const std::vector<std::vector<int>>& second) {
        if (first.empty() || second.empty() || first[0].size() != second.size()) {
            throw std::invalid_argument("Matrix dimensions do not match for multiplication.");
        }

        int firstRows = first.size();
        int firstCols = first[0].size();
        int secondCols = second[0].size();

        std::vector<std::vector<int>> result(firstRows, std::vector<int>(secondCols, 0));

        for (int i = 0; i < firstRows; ++i) {
            for (int j = 0; j < secondCols; ++j) {
                for (int k = 0; k < firstCols; ++k) {
                    result[i][j] += first[i][k] * second[k][j];
                }
            }
        }

        return result;
    }

    static void PrintMatrix(const std::vector<std::vector<int>>& matrix) {
        for (const auto& row : matrix) {
            for (int elem : row) {
                std::cout << elem << " ";
            }
            std::cout << std::endl;
        }
    }
};
